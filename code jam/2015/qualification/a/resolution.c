#include <stdio.h>

int main() {
    int i, j, numberOfCases, maxShyLevel, clappingPeople, neededPeople, newPeople;

    char input[2];

    FILE *file;
    file = fopen("input.txt","r");

    fscanf(file, "%i\n", &numberOfCases);

    int caseArray[numberOfCases][9];
    int result[numberOfCases];

    for (i = 0; i < numberOfCases; i++)
    {
        fgets(input, 3, file);
        maxShyLevel = atoi(input);

        fgets(input, 2, file);
        clappingPeople = atoi(input);

        result[i] = 0;
        for (j = 1; j <= maxShyLevel; ++j)
        {
            fgets(input, 2, file);
            newPeople = atoi(input);
            if (j > clappingPeople && newPeople > 0 ) {
                result[i] += j - clappingPeople;
                clappingPeople += j - clappingPeople;
            }
            clappingPeople += newPeople;
        }
        fscanf(file, "\n");
    }

    fclose(file);

    file = fopen("output.txt","w");

    for (i = 0; i < numberOfCases; i++)
    {
        fprintf(file, "Case #%i: %i", i+1, result[i]);
        if (i != numberOfCases-1) {
            fprintf(file, "\n");
        }
    }

    fclose(file);
    return 0;
}
