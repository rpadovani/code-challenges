#include <stdio.h>
#include <math.h>

int main() {
    int i, j, numberOfCases, numberOfDiners, totalPancakes, medium, final, step;

    char input[2];

    FILE *file;
    file = fopen("input.txt","r");

    fscanf(file, "%i\n", &numberOfCases);

    int result[numberOfCases];
    int numberOfPancakes[1000];

    for (i = 0; i < numberOfCases; i++)
    {
        result[i] = 0;
        final = 0;
        totalPancakes = 0;
        fscanf(file, "%i\n", &numberOfDiners);
        for (j = 0; j < numberOfDiners; j++) {
            fscanf(file, "%i", &numberOfPancakes[j]);
            totalPancakes += numberOfPancakes[j];
        }
        fscanf(file, "\n");

        medium = (totalPancakes+ (numberOfDiners/2)) /numberOfDiners;
        for (j = 0; j < numberOfDiners; j++) {
            step = 0;
            while (numberOfPancakes[j] / 2 > medium || numberOfPancakes[j] * 2 > totalPancakes + 1) {
                result[i] += pow(2, step);
                step++;
                numberOfPancakes[j] = (numberOfPancakes[j] + 1) /2 ;
            }

            if (numberOfPancakes[j] > final) {
                final = numberOfPancakes[j];
            }
        }

        result[i] += final;
    }

    fclose(file);

    file = fopen("output.txt","w");

    for (i = 0; i < numberOfCases; i++)
    {
        fprintf(file, "Case #%i: %i", i+1, result[i]);
        if (i != numberOfCases-1) {
            fprintf(file, "\n");
        }
    }

    fclose(file);
    return 0;
}
