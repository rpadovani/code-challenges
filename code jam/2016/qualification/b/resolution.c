#include <stdio.h>
#include <string.h>

int main() {
    int i, n, numberOfCases;

    char lastChar;

    FILE *file;
    file = fopen("input.txt","r");

    fscanf(file, "%i\n", &numberOfCases);

    int result[numberOfCases];
    char string[101];

    for (i = 0; i < numberOfCases; i++)
    {
        result[i] = 0;
        fscanf(file, "%s\n", string);
        n = strlen(string);
        n--;
        lastChar = '+';
        while (n >= 0) {
            if (string[n] != lastChar) {
                lastChar = string[n];
                result[i]++;
            }
            n--;
        }
    }

    fclose(file);

    file = fopen("output.txt","w");

    for (i = 0; i < numberOfCases; i++)
    {
        fprintf(file, "Case #%i: %i", i+1, result[i]);
        if (i != numberOfCases-1) {
            fprintf(file, "\n");
        }
    }

    fclose(file);
    return 0;
}
