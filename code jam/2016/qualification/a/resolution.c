#include <stdio.h>

int main() {
    int i, j, k, l, numberOfCases;

    FILE *file;
    file = fopen("input.txt","r");

    fscanf(file, "%i\n", &numberOfCases);

    int result[numberOfCases];

    int number;
    int splitNumber;
    int digit[10];
    int singleDigit;
    int sum;

    for (i = 0; i < numberOfCases; i++) {
        fscanf(file, "%i\n", &number);
        for (k = 0; k < 10; k++) { digit[k] = 0; }
        result[i] = -1;

        for (j = 1; j <= 100; j++) {
            splitNumber = number * j;
            while (splitNumber >= 1) {
                singleDigit = splitNumber % 10;
                digit[singleDigit] = 1;
                sum = 0;
                for (l = 0; l < 10; l++) {
                    sum += digit[l];
                }
                if (sum == 10) {
                    result[i] = number * j;
                    break;
                }
                splitNumber /= 10;
            }

            if (sum == 10) { break; }
        }
    }

    fclose(file);

    file = fopen("output.txt","w");

    for (i = 0; i < numberOfCases; i++)
    {
        if (result[i] != -1) {
            fprintf(file, "Case #%i: %i", i+1, result[i]);
        } else {
            fprintf(file, "Case #%i: INSOMNIA", i+1);
        }

        if (i != numberOfCases-1) {
            fprintf(file, "\n");
        }
    }

    fclose(file);
    return 0;
}
