#include <stdio.h>
#include <math.h>
#include <string.h>
#include <gmp.h>

#define MAX 10000

void convertFrom2ToN(mpz_t n, int b, mpz_t r) {
    int j = 0;
    mpz_t _n, result;
    mpz_t division, _d, power;
    mpz_init_set_si(division, 0);
    mpz_init_set_si(_n, 0);
    mpz_init_set_si(_d, 0);
    mpz_init_set_si(result, 0);
    mpz_set(_n, n);
    mpz_init_set_si(power, 0);

    while (mpz_cmp_si(_n, 1) >= 0) {
        mpz_mod_ui(division, _n, 10);
        mpz_ui_pow_ui(power, b, j);
        mpz_mul(_d, division, power);
        mpz_add(result, result, _d);
        mpz_tdiv_q_ui(_n, _n, 10);
        j++;
    }
    mpz_set(r, result);
}
long long int primeNumbers[MAX];
int lastInserted = 0;

int isPrime(long long int n) {
    if (n == 1 || n == 2 || n == 3) {return 0;}
    int j = 2;
    int lastIndex = 0;

    while (j*j < n) {
        if (n % j == 0) { return 1; }
        if (lastIndex < lastInserted) {
            j = primeNumbers[lastIndex];
            lastIndex++;
        } else {
            j += 2;
        }
    }

    return 0;
}

int generatePrimeNumber(int n) {
    n++;
    if (n == 2 || n == 3) { return n; }
    if (n % 2 == 0) { n++; }

    int i = 0;
    while (i < lastInserted && i < MAX) {
        if (primeNumbers[i] >= n) { return primeNumbers[i]; }
        i++;
    }

    if (lastInserted == MAX) {return -2;}

    int k = 0;
    while (1 == 1) {
        if ((k = isPrime(n)) == 0) {
            if (lastInserted < MAX) {
                primeNumbers[lastInserted] = n;
                lastInserted++;
            }
            return n;
        } else if (k == 2) {
            return -2;
        }
        n += 2;
    }
}

int isNotPrime(mpz_t n) {
    int j = generatePrimeNumber(1);

    while (mpz_cmp_ui(n, j*j) >= 0) {
        if (mpz_divisible_ui_p(n, j) != 0) { return j;}
        j = generatePrimeNumber(j);
        if (j == -2) { return -2; }
    }

    return -1;
}

void convertBinary(long long int n, mpz_t m) {
    mpz_t j, k;
    mpz_init_set_si(j, 1);
    mpz_init_set_si(k, 1);
    while (n >= 1) {
        mpz_mul_si(k, j, n%2);
        mpz_add(m, m, k);
        n /= 2;
        mpz_mul_ui(j, j, 10);
    }
}

void generatePossibleJam(mpz_t n, int l) {
    mpz_t number, asd, power;
    long long int startingPoint = 0;
    mpz_init_set_str(number, "0", 10);
    mpz_init_set_str(asd, "0", 10);
    mpz_init_set_str(power, "0", 10);
    mpz_ui_pow_ui(power, 10, l-1);
    while (mpz_cmp(number, n) <= 0) {
        mpz_set_ui(number, 0);
        mpz_set_ui(asd, 0);
        convertBinary(startingPoint, asd);
        mpz_add_ui(number, number, 1);
        mpz_mul_ui(asd, asd, 10);
        mpz_add(number, number, power);
        mpz_add(number, number, asd);
        startingPoint++;
    }
    mpz_set(n, number);
}

int main() {
    int i,  numberOfCases;

    FILE *file;
    file = fopen("input.txt","r");

    fscanf(file, "%i\n", &numberOfCases);
    int data[numberOfCases][2];

    for (i = 0; i < numberOfCases; i++) {
        fscanf(file, "%i %i\n", &data[numberOfCases][0], &data[numberOfCases][1]);
    }
    fclose(file);

    file = fopen("output.txt","w");
    mpz_t jam;
    mpz_init(jam);
    mpz_set_str(jam, "0", 10);
    int numberOfResults, a, b, c, d, e, f, g, h, ii;
    mpz_t _a, _b, _c, _d, _e, _f, _g, _h, _ii;
    mpz_init_set_str(_a, "0", 10);
    mpz_init_set_str(_b, "0", 10);
    mpz_init_set_str(_c, "0", 10);
    mpz_init_set_str(_d, "0", 10);
    mpz_init_set_str(_e, "0", 10);
    mpz_init_set_str(_f, "0", 10);
    mpz_init_set_str(_g, "0", 10);
    mpz_init_set_str(_h, "0", 10);
    mpz_init_set_str(_ii, "0", 10);

    for (i = 0; i < numberOfCases; i++) {
        fprintf(file, "Case #%i:\n", i+1);
        numberOfResults = 0;
        while (numberOfResults < data[numberOfCases][1]) {
            generatePossibleJam(jam, data[numberOfCases][0]);
            convertFrom2ToN(jam, 2, _a);
            convertFrom2ToN(jam, 3, _b);
            convertFrom2ToN(jam, 4, _c);
            convertFrom2ToN(jam, 5, _d);
            convertFrom2ToN(jam, 6, _e);
            convertFrom2ToN(jam, 7, _f);
            convertFrom2ToN(jam, 8, _g);
            convertFrom2ToN(jam, 9, _h);
            convertFrom2ToN(jam, 10, _ii);

            if ((a = isNotPrime(_a)) > 0 &&
                (b = isNotPrime(_b)) > 0 &&
                (c = isNotPrime(_c)) > 0 &&
                (d = isNotPrime(_d)) > 0 &&
                (e = isNotPrime(_e)) > 0 &&
                (f = isNotPrime(_f)) > 0 &&
                (g = isNotPrime(_g))> 0 &&
                (h = isNotPrime(_h)) > 0 &&
                (ii = isNotPrime(_ii)) > 0) {
                        mpz_out_str(file, 10, jam);

                        fprintf(file, " %i %i %i %i %i %i %i %i %i\n",
                            a, b, c, d, e, f, g, h, ii);
                        numberOfResults++;
                }
        }
    }

    fclose(file);
    return 0;
}
