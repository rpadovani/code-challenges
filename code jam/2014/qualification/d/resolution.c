#include <stdio.h>

void array_reorder(float array[100][1000], int i, int n) {
	int q, w, e;
	float t;

	for (q = n; q > 0; q--)
	{
		for (w = 1; w < q; ++w)
		{

				if (array[i][w-1] > array[i][w])
				{
					t = array[i][w];
					array[i][w] = array[i][w-1];
					array[i][w-1] = t;
				}
		}
	}
}


int main() {
	char null;

	FILE *file;
	file = fopen("input.txt","r");

	int numberOfCase, i, j, e, r, numberOfBlocks;

    fscanf(file, "%i\n", &numberOfCase);

    float noemiBlocks[numberOfCase][1000];
	float kenBlocks[numberOfCase][1000];
	int war[numberOfCase];
	int deceitful[numberOfCase];

    for (i = 0; i < numberOfCase; ++i)
    {
    	fscanf(file, "%i\n", &numberOfBlocks);
	    for (j = 0; j < numberOfBlocks; ++j)
	    {
	    	fscanf(file, "%f", &noemiBlocks[i][j]);
	    	fscanf(file, "%c", &null);
	    }

	   	for (j = 0; j < numberOfBlocks; ++j)
	    {
	    	fscanf(file, "%f", &kenBlocks[i][j]);
	    	fscanf(file, "%c", &null);
	    }

	    array_reorder(noemiBlocks, i, numberOfBlocks);
	    array_reorder(kenBlocks, i, numberOfBlocks);

	    war[i] = 0;
	    for (e = 0, r = 0; e < numberOfBlocks && r < numberOfBlocks; r++)
	    {

	    	if (noemiBlocks[i][e] > kenBlocks[i][r]) {
	    		war[i]++;
	    	}
	    	else {
	    		e++;
	    	}
	    }

	    deceitful[i] = 0;

	   	for (e = numberOfBlocks -1, r = numberOfBlocks-1; e >=0 && r >= 0; r--)
	    {

	    	if (noemiBlocks[i][e] > kenBlocks[i][r]) {
	    		deceitful[i]++;
	    		e--;
	    	}
	    }
	}

	fclose(file);


    file = fopen("output.txt","w");

    for (i = 0; i < numberOfCase; ++i)
    {
    	fprintf(file, "Case #%i: %i %i", i+1, deceitful[i], war[i]);

    	if (i != numberOfCase - 1)
    	{
            fprintf(file, "\n");
    	}
    }

    fclose(file);
    return 0;
}
