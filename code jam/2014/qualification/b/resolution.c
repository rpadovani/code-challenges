#include <stdio.h>

int main() {
	char null;

	FILE *file;
	file = fopen("input.txt","r");

	int numberOfCase, i, j;

    fscanf(file, "%i\n", &numberOfCase);

    double cookies[numberOfCase][3];
    double times[numberOfCase];
    double howMuchWithoutFactory, noMoreFarm, howMuchTimeNow, howMuchProductionWithFarm, howMuchTimeToBuildAFarm, farm, howMuchTimeThen, howMuchTimeToBuildAllFarm;

    for (i = 0; i < numberOfCase; ++i)
    {
    	fscanf(file, "%lf %lf %lf\n", &cookies[i][0], &cookies[i][1], &cookies[i][2]);
    	howMuchTimeNow = cookies[i][2] / 2;
    	howMuchProductionWithFarm = 2;
    	howMuchTimeToBuildAllFarm = 0;
    	noMoreFarm = 0;
    	farm = 0;
    	while (noMoreFarm != 1) {
    		howMuchTimeToBuildAFarm = cookies[i][0] / howMuchProductionWithFarm;
    		howMuchTimeToBuildAllFarm += howMuchTimeToBuildAFarm;
    		howMuchProductionWithFarm += cookies[i][1];
    		howMuchTimeThen = howMuchTimeToBuildAllFarm + cookies[i][2] / howMuchProductionWithFarm;

    		if (howMuchTimeThen > howMuchTimeNow)
    		{
    			noMoreFarm = 1;
    		}
    		else {
    			howMuchTimeNow = howMuchTimeThen;
    			//printf("%f\n", howMuchTimeNow );
    			farm++;
    		}
    	}
    	times[i] = howMuchTimeNow;
    	//printf("%f\n", times[i] );
    	/*howMuchProductionWithFarm = 2;
    	for (j = 0; j <= farm; ++j)
    	{
    		times[i] += cookies[numberOfCase][0] / howMuchProductionWithFarm;
    		howMuchProductionWithFarm += cookies[numberOfCase][1];
    	}
    	times[i] += cookies[numberOfCase][2] / howMuchProductionWithFarm;*/
    }
    fclose(file);

    file = fopen("output.txt","w");

    for (i = 0; i < numberOfCase; ++i)
    {
    	fprintf(file, "Case #%i: %.7lf", i+1, times[i]);

    	if (i != numberOfCase - 1)
    	{
            fprintf(file, "\n");
    	}
    }

    fclose(file);
    return 0;
}
