#include <stdio.h>

int main() {	
	int i, j, k, numberOfCase, firstCase, secondCase;

	char null;

	FILE *file;
	file = fopen("input.txt","r");

    fscanf(file, "%i\n", &numberOfCase);

    int caseArray[numberOfCase*2][4][4];
    int coincidence[numberOfCase*2];
    int numberOfCoincidence[numberOfCase*2];

    for (i = 0; i < numberOfCase * 2 - 1; i+=2)
    {
    	fscanf(file, "%i\n", &firstCase);
    	//printf("%i\n", firstCase);

        for (j = 0; j < 4; ++j) 
        {
    		for (k = 0; k < 4; ++k)
    		{
    			fscanf(file, "%i", &caseArray[i][j][k]);
            	fscanf(file, "%c", &null);
    		}
            //fscanf(file, "%c", &null);
    	}
    	//fscanf(file, "%c", &null);

/*    	for (j = 0; j < 4; ++j) 
        {
    		for (k = 0; k < 4; ++k)
    		{
    			printf("%i ", caseArray[i][j][k]);
    		}
            printf("\n");
    	}
    	printf("\n");
*/
    	fscanf(file, "%i\n", &secondCase);
    	// printf("%i\n", secondCase);

        for (j = 0; j < 4; ++j) 
        {
    		for (k = 0; k < 4; ++k)
    		{
    			fscanf(file, "%i", &caseArray[i+1][j][k]);
            	fscanf(file, "%c", &null);
    		}
            //fscanf(file, "%c", &null);
    	}
        //fscanf(file, "%c", &null);

/*        for (j = 0; j < 4; ++j) 
        {
    		for (k = 0; k < 4; ++k)
    		{
    			printf("%i ", caseArray[i+1][j][k]);
    		}
            printf("\n");
    	}
    	printf("\n");*/

        numberOfCoincidence[i] = 0;

        for (j = 0; j < 4; ++j)
        {
        	for (k = 0; k < 4; ++k) 
        	{
        		if (caseArray[i][firstCase-1][j] == caseArray[i+1][secondCase-1][k]) 
        		{
        			coincidence[i] = caseArray[i][firstCase-1][j];
        			numberOfCoincidence[i]++;
        			//printf("Case %i: %i\n", i, numberOfCoincidence[i]);
        		}
        	}
        }
    }

    fclose(file);

    file = fopen("output.txt","w");

    for (i = 0; i < numberOfCase * 2 - 1; i+=2)
    {
    	if (numberOfCoincidence[i] == 0)
    	{
    		fprintf(file, "Case #%i: Volunteer cheated!", (i+2)/2);
    	}
    	else if (numberOfCoincidence[i] == 1)
    	{
    		fprintf(file, "Case #%i: %i", (i+2)/2, coincidence[i]);
    	}
    	else 
    	{
    		fprintf(file, "Case #%i: Bad magician!", (i+2)/2);
    	}

    	if (i != numberOfCase * 2 - 2)
    	{
            fprintf(file, "\n");
    	}
    }

    fclose(file);
    return 0;
}
