#include <stdio.h>

int is_palindrom(int j) {
    int i = 0;
    if (j < 10)
        return 1;

    int k = j;

    while (k >= 10) {
        k /= 10; 
        i++;
    }


    int lunghezza = i;
    int cifre[lunghezza];

    i = 0;
    
    while (j >= 1) {
        cifre[i] = j % 10;
        j /= 10; 
        i++;
    }


    i = 0;

    while (i < lunghezza - i) {
        if (cifre[i] != cifre[lunghezza-i])
            return 0;
        i++;
    }

    return 1;
}


int is_square(int x) {    
    int i = 1;
    for (i = 1; i*i < x; i++) {
        if ((i*i) == x) {
            if (is_palindrom(i) == 1)
                return 1;
            else
                return 0;
            }
        }
    return 0;
}


int main (void) 
{
	int i, j, risultato, numeroCasi;

	FILE *file;
	file = fopen("input.txt","r");

    fscanf(file, "%i\n", &numeroCasi);

    int arrayCasi[numeroCasi][2];

    for (i = 0; i < numeroCasi; i++)
    	fscanf(file, "%i %i\n", &arrayCasi[i][0], &arrayCasi[i][1]);

    fclose(file);

    file = fopen("output.txt","w");

    for (i = 0; i < numeroCasi; i++) {	
        risultato = 0;
    	for (j = arrayCasi[i][0]; j <= arrayCasi[i][1]; j++) {
            if (is_palindrom(j) == 1)
                if (is_square(j) == 1)
                    risultato++;
        }

        fprintf(file, "Case #%i: %i", i+1, risultato);

        if (i != numeroCasi - 1)
            fprintf(file, "\n");
    }

    fclose(file);
    return 0;
}