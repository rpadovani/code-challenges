#include <stdio.h>

int controlla_vincita(char arrayCasi[4][4], char c) 
{
    int j, k, cont;

    for (j = 0; j < 4; ++j) {
        cont = 0;
        for (k = 0; k < 4; ++k) {
            if (arrayCasi[j][k] == c || arrayCasi[j][k] == 'T')
                cont++;
            if (cont == 4)
                return 1;
        }
    }

    for (j = 0; j < 4; ++j) {
        cont = 0;
        for (k = 0; k < 4; ++k) {
            if (arrayCasi[k][j] == c || arrayCasi[k][j] == 'T')
                cont++;
            if (cont == 4)
                return 1;
        }
    }


    cont = 0;
    for (j = 0; j < 4; ++j) {
        if (arrayCasi[j][j] == c || arrayCasi[j][j] == 'T')
            cont++;
        if (cont == 4)
            return 1;
    }

    cont = 0;
    for (j = 0; j < 4; ++j) {
        if (arrayCasi[3-j][j] == c || arrayCasi[3-j][j] == 'T')
            cont++;
        if (cont == 4)
            return 1;
    }

    return 0;
}

int controlla_punti (char arrayCasi[4][4])
{
    int k, j;

    for (j = 0; j < 4; ++j) {
        for (k = 0; k < 4; ++k) {
            if (arrayCasi[j][k] == '.')
                return 1;
        }
    }
    return 0;    
}

int main (void) 
{
	int i, j, k, numeroCasi;
	int esisteT = 0;
    char null;

	FILE *file;
	file = fopen("input.txt","r");

    fscanf(file, "%i\n", &numeroCasi);

    char arrayCasi[numeroCasi][4][4];

    for (i = 0; i < numeroCasi; i++) {
        for (j = 0; j < 4; ++j) {
    		for (k = 0; k < 4; ++k)
    			fscanf(file, "%c", &arrayCasi[i][j][k]);
            fscanf(file, "%c", &null);
    	}
        fscanf(file, "%c", &null);
    }

    fclose(file);

    file = fopen("output.txt","w");

    for (i = 0; i < numeroCasi; i++) {	
    	if (controlla_vincita(arrayCasi[i], 'X') == 1 )
    		fprintf(file, "Case #%i: X won", i+1);
    	else if (controlla_vincita(arrayCasi[i], 'O') == 1)
    		fprintf(file, "Case #%i: O won", i+1);
    	else if (controlla_punti(arrayCasi[i]) == 1)
            fprintf(file, "Case #%i: Game has not completed", i+1);
        else
    		fprintf(file, "Case #%i: Draw", i+1);

        if (i != numeroCasi - 1)
            fprintf(file, "\n");
    }

    fclose(file);
    return 0;
}