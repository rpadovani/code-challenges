#include <stdio.h>

// 0 for false, 1 for true

int isSquare(int number) {
	int i;
	for (i = 1; i <= number / 2; ++i) {
		if (i * i == number)
			return 1;
	}

	return 0;
}

int checkSquare(char *square, int side) {
	int i, k, numberOfSharps = 0;
	int firstSharpX, firstSharpY, lastSharpX, lastSharpY;
	for (i = 0; i < side; ++i) {
		for (k = 0; k < side; ++k) {
			if (square[i * side + k] == '#')
				numberOfSharps++;
		}
	}

	if (isSquare(numberOfSharps) == 0)
		return 0;

	for (i = 0; i < side; ++i) {
		for (k = 0; k < side; ++k) {
			if (square[i * side + k] == '#')
				firstSharpX = i;
				firstSharpY = k;
				break;
		}
	}

	for (i = 0; i < side; ++i) {
		for (k = 0; k < side; ++k) {
			if (square[i * side + k] == '#')
				lastSharpX = i;
				lastSharpY = k;
		}
	}

	for (i = firstSharpX; i <= lastSharpX; ++i) {
		for (k = firstSharpY; k <= lastSharpY; ++k) {
			if (square[i * side + k] != '#')
				return 0;
		}
	}

	return 1;
}

int main(void) {
	int numberCase, numberSides;
	int i, j, k;
	char null;
	FILE *file, *output;
	file = fopen("input.txt","r");
	output = fopen("output.txt", "w");

    fscanf(file, "%i\n", &numberCase);

    for (i = 0; i < numberCase; i++) {
    	fscanf(file, "%i\n", &numberSides);
    	char square[numberSides][numberSides];
    	for (j = 0; j < numberSides; ++j) {
    		for (k = 0; k < numberSides; ++k) {
    			fscanf(file, "%c", &square[j][k]);
    		}
            fscanf(file, "%c", &null);
    	}

    	if (checkSquare(&square[0][0], numberSides) == 1)
    		fprintf(output, "Case #%i: YES", i+1);
    	else
    		fprintf(output, "Case #%i: NO", i+1); 

    	if (i != numberCase - 1)
    		fprintf(output, "\n");
    }

    fclose(file);
    fclose(output);

    return 0;
}