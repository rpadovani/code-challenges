#include <stdio.h>
#include <math.h>

int main() {
    FILE *file;
    file = fopen("input.txt","r");

    fscanf(file, "%i\n", &numberOfCases);
    fclose(file);

    file = fopen("output.txt","w");

    for (i = 0; i < numberOfCases; i++)
    {
        fprintf(file, "Case #%i: %i", i+1, result[i]);
        if (i != numberOfCases-1) {
            fprintf(file, "\n");
        }
    }

    fclose(file);
    return 0;
}
