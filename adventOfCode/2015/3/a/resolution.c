#include <stdio.h>
#include <math.h>

int main() {
    FILE *file;
    file = fopen("input.txt","r");

    int max = 262144;

    int bigArray[max];
    int startingPoint = max/2;
    char pos;

    int i;
    for (i = 0; i < max; i++) {
        bigArray[i] = 0;
    }

    int actualX = 0;
    int actualY = 0;
    int actualXR = 0;
    int actualYR = 0;

    int molt;

    bigArray[startingPoint] = 1;

    int houseVisited = 1;
    i = 0;
    while (fscanf(file, "%c", &pos) != -1) {

        if (i % 2 == 0) {
            if (pos == '>') {
                actualX++;
            } else if (pos == '^') {
                actualY++;
            } else if (pos == 'v') {
                actualY--;
            } else if (pos == '<') {
                actualX--;
            }
            molt = startingPoint + actualX + 512 * actualY;
        } else {
            if (pos == '>') {
                actualXR++;
            } else if (pos == '^') {
                actualYR++;
            } else if (pos == 'v') {
                actualYR--;
            } else if (pos == '<') {
                actualXR--;
            }
            molt = startingPoint + actualXR + 512 * actualYR;
        }

      if (molt > max || molt < 0) {
          printf("Too little\n");
          break;
      } else {
          if (bigArray[molt] == 0) {
              houseVisited++;
              bigArray[molt] = 1;
          }
      }

      i++;
    }

    // file = fopen("output.txt","w");

    printf("%i\n", houseVisited);
    // for (i = 0; i < numberOfCases; i++)
    // {
    //     fprintf(file, "Case #%i: %i", i+1, result[i]);
    //     if (i != numberOfCases-1) {
    //         fprintf(file, "\n");
    //     }
    // }

    // fclose(file);
    return 0;
}
