#include <stdio.h>
#include <math.h>

int main() {
    FILE *file;
    file = fopen("input.txt","r");

    int l, w, h;
    char u;

    int total = 0;

    int ribbon = 0;

    while (fscanf(file, "%i%c%i%c%i\n", &l, &u, &w, &u, &h) != -1) {
      total += l * w * h;

      total += l < w ?
                      h < w ?
                              l + l + h + h :
                              l + l + w + w :
                      h < l ?
                              w + w + h + h:
                              w + w + l + l;

    }

    // file = fopen("output.txt","w");

    printf("%i\n", total);
    // for (i = 0; i < numberOfCases; i++)
    // {
    //     fprintf(file, "Case #%i: %i", i+1, result[i]);
    //     if (i != numberOfCases-1) {
    //         fprintf(file, "\n");
    //     }
    // }

    // fclose(file);
    return 0;
}
