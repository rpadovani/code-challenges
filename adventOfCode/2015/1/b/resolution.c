#include <stdio.h>
#include <math.h>

int main() {
    FILE *file;
    file = fopen("input.txt","r");

    char instructions[2048];

    fscanf(file, "%s\n", instructions);
    fclose(file);

    int i = 0;
    int currentFloor = 0;

    while (instructions[i] == '(' || instructions[i] == ')') {
        if (instructions[i] == '(') {
            currentFloor++;
        } else {
            currentFloor--;
        }

        i++;

        if (currentFloor < 0) {
            printf("%i\n", i);
            break;
        }
    }

    // file = fopen("output.txt","w");

    printf("%i\n", currentFloor);
    // for (i = 0; i < numberOfCases; i++)
    // {
    //     fprintf(file, "Case #%i: %i", i+1, result[i]);
    //     if (i != numberOfCases-1) {
    //         fprintf(file, "\n");
    //     }
    // }

    // fclose(file);
    return 0;
}
